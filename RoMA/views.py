from django.http import HttpResponse
from django.template import loader

def root(request):
    template = loader.get_template('root.html')
    context = {
        'title': 'Welcome',
    }
    return HttpResponse(template.render(context, request))
