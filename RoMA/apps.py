from django.apps import AppConfig


class RoMAConfig(AppConfig):
    name = 'RoMA'
