from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='samples-index'),
    path('<str:sample_name>/', views.detail, name='sample-detail'),
    path('<str:sample_name>/<int:id>_refined.csv', views.reflectance_data_corr, name='sample-reflectance-data-corr'),
    path('<str:sample_name>/<int:id>_calibrated.csv', views.reflectance_data_raw, name='sample-reflectance-data-raw'),
    path('<str:sample_name>/<int:id>_reflectance.svg', views.reflectance_fig, name='sample-reflectance-fig'),
    path('<str:sample_name>/<int:id>_xrd.csv', views.xrd_data, name='sample-xrd-data'),
    path('<str:sample_name>/<int:id>_xrd.svg', views.xrd_fig, name='sample-xrd-fig')
]
