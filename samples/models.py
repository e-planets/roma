from django.db import models


class Reflectance(models.Model):
    id = models.AutoField(db_column='Id', blank=True, null=False, primary_key=True)  # Field name made lowercase.
    sample_name = models.TextField(db_column='Sample name')  # Field name made lowercase. Field renamed to remove unsuitable characters.
    sample_aspect = models.TextField(db_column='Sample aspect', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    calibrated_spectrum = models.TextField(db_column='Calibrated spectrum', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    corrected_spectrum = models.TextField(db_column='Corrected spectrum', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    error = models.TextField(db_column='Error', blank=True, null=True)  # Field name made lowercase.
    continuum_removed_spectrum = models.TextField(db_column='Continuum-removed spectrum', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    spectral_resolution = models.TextField(db_column='Spectral resolution', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    band_parameters = models.TextField(db_column='Band parameters', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    incidence = models.IntegerField(db_column='Incidence', blank=True, null=True)  # Field name made lowercase.
    emergence = models.IntegerField(db_column='Emergence', blank=True, null=True)  # Field name made lowercase.
    azimut = models.IntegerField(db_column='Azimut', blank=True, null=True)  # Field name made lowercase.
    spot_size = models.TextField(db_column='Spot size', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    date = models.CharField(db_column='Date', blank=True, null=True, max_length=25)  # Field name made lowercase.
    experiment = models.TextField(db_column='Experiment', blank=True, null=True)  # Field name made lowercase.
    instrument = models.CharField(db_column='Instrument', blank=True, null=True, max_length=256)  # Field name made lowercase.
    operator = models.CharField(db_column='Operator', blank=True, null=True, max_length=25)  # Field name made lowercase.
    target_image = models.BinaryField(db_column='Target image', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.

    class Meta:
        managed = False
        db_table = 'Reflectance'


class Sample(models.Model):
    name = models.TextField(db_column='Name', blank=True, null=False, primary_key=True)  # Field name made lowercase.
    type = models.TextField(db_column='Type', blank=True, null=True)  # Field name made lowercase.
    aspect = models.TextField(db_column='Aspect', blank=True, null=True)  # Field name made lowercase.
    origin = models.TextField(db_column='Origin', blank=True, null=True)  # Field name made lowercase.
    owner = models.TextField(db_column='Owner', blank=True, null=True)  # Field name made lowercase.
    cite = models.TextField(db_column='Cite', blank=True, null=True)  # Field name made lowercase.
    image = models.BinaryField(db_column='Image', blank=True, null=True)  # Field name made lowercase.
    image2 = models.BinaryField(db_column='Image2', blank=True, null=True)  # Field name made lowercase.
    image3 = models.BinaryField(db_column='Image3', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'Sample'


class Xrd(models.Model):
    id = models.AutoField(db_column='Id', blank=True, null=False, primary_key=True)  # Field name made lowercase.
    sample_name = models.TextField(db_column='Sample name')  # Field name made lowercase. Field renamed to remove unsuitable characters.
    xrd_id = models.TextField(db_column='XRD ID', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    diffractogram = models.TextField(db_column='Diffractogram', blank=True, null=True)  # Field name made lowercase.
    first_order_composition = models.BinaryField(db_column='First-order composition', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    date = models.TextField(db_column='Date', blank=True, null=True)  # Field name made lowercase.
    instrument = models.TextField(db_column='Instrument', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'XRD'