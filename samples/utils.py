import matplotlib
matplotlib.use('Agg')
from matplotlib import pyplot

from io import BytesIO

import matplotlib as mpl

mpl.rcParams['axes.linewidth'] = 0.5
mpl.rcParams['font.family'] = 'sans-serif'
mpl.rcParams['font.sans-serif'] = 'Microsoft Tai Le'

def plot_spectrum(title, calibrated_spectrum_values, corrected_spectrum_values, continuum_removed_spectrum_values):
    if 'pellet' in title:
        fig = pyplot.figure(figsize=(7,3))
        ax1 = fig.add_subplot(111)
    else:
        fig = pyplot.figure(figsize=(7, 6))
        ax1 = fig.add_subplot(211)
        ax2 = fig.add_subplot(212)
        
    fig.suptitle(title, weight='bold')

    if 'rock' in title or 'pellet' in title:
        color = 'cornflowerblue'
    if 'powder' in title:
        color = 'tomato'

    # ax1.plot(calibrated_spectrum_values[0::2], calibrated_spectrum_values[1::2], label='calibrated', color='k')
    ax1.plot(corrected_spectrum_values[0::2], corrected_spectrum_values[1::2], color=color)
    ax1.set_ylabel('Reflectance')
    ax1.legend(handlelength=1, frameon=False)
    
    if 'pellet' not in title:
        ax2.plot([min(corrected_spectrum_values[0::2]),max(corrected_spectrum_values[0::2])],[1,1], ls='--', lw=.7, color='silver')
        ax2.plot(continuum_removed_spectrum_values[0::2], continuum_removed_spectrum_values[1::2], color=color)
        ax2.set_xlabel('Wavelength (nm)'), ax2.set_ylabel('Continuum-removed reflectance')
        
        ax2.tick_params(axis='both', which = 'both', width =0.5, direction='in', top=True, right=True)
        ax2.minorticks_on()
        
        ax1.tick_params(axis='both', which = 'both', labeltop=True, labelbottom=False)
   
    ax1.tick_params(axis='both', which = 'both', width =0.5, direction='in', top=True, right=True)
    ax1.minorticks_on()
    
    
    fig.tight_layout()
    f = BytesIO()
    fig.savefig(f, format='svg')
    return f.getvalue().decode('utf-8')

def plot_diffractogram(title, diffractogram_values):
    fig = pyplot.figure(figsize=(15, 4))
    fig.suptitle(title, weight='bold')

    ax1 = fig.add_subplot(111)
    ax1.plot(diffractogram_values[0::2], diffractogram_values[1::2], color='gold', lw=1)
    ax1.set_xlabel('2Ɵ (°)'), ax1.set_ylabel('Counts')
    
    ax1.tick_params(axis='both', which = 'both', width =0.5, direction='in', top=True, right=True)
    ax1.minorticks_on()
        
    fig.tight_layout()
    f = BytesIO()
    fig.savefig(f, format='svg')
    return f.getvalue().decode('utf-8')
