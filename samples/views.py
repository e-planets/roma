from django.http import FileResponse, HttpResponse
from django.template import loader

from .models import Sample, Reflectance, Xrd

import base64

from .utils import plot_spectrum, plot_diffractogram

def index(request):
    template = loader.get_template('index.html')
    context = {
        'title': 'Sample index',
        'samples': Sample.objects.using('library').order_by('name')
    }
    return HttpResponse(template.render(context, request))

def detail(request, sample_name):
    template = loader.get_template('detail.html')

    sample = Sample.objects.using('library').get(name=sample_name)
    if sample.image and len(sample.image) > 0:
        sample.image = base64.b64encode(sample.image).decode('utf-8')
    if sample.image2 and len(sample.image2) > 0:
        sample.image2 = base64.b64encode(sample.image2).decode('utf-8')
    if sample.image3 and len(sample.image3) > 0:
        sample.image3 = base64.b64encode(sample.image3).decode('utf-8')


    reflectances = Reflectance.objects.using('library').filter(sample_name=sample_name)
    for r in reflectances:
        if r.band_parameters:        
            r.band_parameters = list(zip(*[iter(r.band_parameters.split())] * 5)) # I'd rather not talk about this

    xrds = Xrd.objects.using('library').filter(sample_name=sample_name)

    context = {
        'title': sample_name,
        'sample': sample,
        'reflectances': reflectances,
        'xrds': xrds
    }
    return HttpResponse(template.render(context, request))

def reflectance_data_corr(request, sample_name, id):
    template = loader.get_template('reflectance.corr.csv')

    reflectance = Reflectance.objects.using('library').filter(sample_name=sample_name).get(id=id)
    if not reflectance:
        return HttpResponseNotFound('')

    corrected = list(zip(*[iter(reflectance.corrected_spectrum.split())] * 2))
    continuum_removed = list(zip(*[iter(reflectance.continuum_removed_spectrum.split())] * 2)) if reflectance.continuum_removed_spectrum else [(x[0], None) for x in corrected]
    context = {
        'data': list(zip(corrected, continuum_removed))
    }
    return HttpResponse(template.render(context, request), content_type='text/csv')

def reflectance_data_raw(request, sample_name, id):
    template = loader.get_template('reflectance.raw.csv')

    reflectance = Reflectance.objects.using('library').filter(sample_name=sample_name).get(id=id)
    
    if not reflectance:
        return HttpResponseNotFound('')
    
    calibrated = list(zip(*[iter(reflectance.calibrated_spectrum.split())] * 2))
    error = list(zip(*[iter(reflectance.error.split())] * 2)) if reflectance.error else [(x[0], None) for x in calibrated]
    
    context = {
        'data': list(zip(calibrated, error))
    }
    return HttpResponse(template.render(context, request), content_type='text/csv')

def xrd_data(request, sample_name, id):
    template = loader.get_template('xrd.csv')

    xrd = Xrd.objects.using('library').filter(sample_name=sample_name).get(id=id)
    
    if not xrd:
        return HttpResponseNotFound('')
    
    context = {
        'data': list(zip(*[iter(xrd.diffractogram.split())] * 2))
    }
    return HttpResponse(template.render(context, request), content_type='text/csv')

def reflectance_fig(request, sample_name, id):
    r = Reflectance.objects.using('library').filter(sample_name=sample_name).get(id=id)
    plot = plot_spectrum('%s, %s (i = %d°, e = %d°, a = %d°)' % (sample_name,r.sample_aspect,r.incidence,r.emergence,r.azimut),
                         [float(x) for x in r.calibrated_spectrum.split()] if r.calibrated_spectrum else [],
                         [float(x) for x in r.corrected_spectrum.split()] if r.corrected_spectrum else [],
                         [float(x) for x in r.continuum_removed_spectrum.split()] if r.continuum_removed_spectrum else [])
    return HttpResponse(plot, content_type='image/svg+xml')

def xrd_fig(request, sample_name, id):
    xrd = Xrd.objects.using('library').filter(sample_name=sample_name).get(id=id)
    plot = plot_diffractogram(xrd.sample_name,
                              [float(x) for x in xrd.diffractogram.split()])
    return HttpResponse(plot, content_type='image/svg+xml')
